const markdownContainer = require('markdown-it-container')
const implicitFigures = require('markdown-it-implicit-figures')

const implicit = [
  implicitFigures,
  {
    // <figure data-type="image">, default: false
    dataType: false,
    // <figcaption>alternative text</figcaption>, default: false
    figcaption: false,
    // <figure tabindex="1+n">..., default: false
    tabindex: true,
    // <a href="img.png"><img src="img.png"></a>, default: false
    link: false,
    copyAttrs: true,
  },
]

const {
  section,
  columns,
  column,
  notification,
} = require('./utils/markdownContainers')

module.exports = {
  content: [
    [
      '/blog',
      {
        page: 'blog/_slug',
        permalink: '/blog/:year/:slug',
        generate: ['get', 'getAll'],
        markdown: {
          plugins: {
            implicit,
            section: [markdownContainer, 'section', section],
            columns: [markdownContainer, 'columns', columns],
            column: [markdownContainer, 'column', column],
            notification: [markdownContainer, 'notification', notification],
          },
        },
      },
    ],
  ],
}
