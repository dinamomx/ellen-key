/* eslint-disable global-require */
/* eslint-disable import/no-extraneous-dependencies */
// Cabezera
import schema from './utils/schema'

const isProd = process.env.NODE_ENV === 'production'

const meta = [
  {
    name: 'keywords',
    content: 'Diseño, Contenido, Dinamo',
  },
  {
    name: 'google-site-verification',
    content: '9D3abpNpNL8-c4j51u_kJaDl6SDJWIlStOPKYyRjzAo',
  },
]

const link = [
  // {
  //   href: 'https://fonts.googleapis.com/css?family=Montserrat:400,700,900',
  //   rel: 'stylesheet',
  // },
  {
    type: 'text/plain',
    rel: 'author',
    href: '/humans.txt',
  },
]

export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    meta,
    link,
    titleTemplate: (titleChunk = '') =>
      titleChunk
        ? `${titleChunk} - SummerHills`
        : 'SummerHills Kinder y Nursery House',
    script: [
      {
        innerHTML: JSON.stringify(schema()),
        type: 'application/ld+json',
      },
      {
        src:
          'https://polyfill.io/v2/polyfill.min.js?features=IntersectionObserver',
        body: false,
      },
    ],
    __dangerouslyDisableSanitizers: ['script'],
  },
  /**
   * Variables de entorno para la página
   */
  env: {
    productionDomain: 'summerhills.com.mx',
  },
  /**
   * Opciones de vue-router
   */
  router: {
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'is-active--exact',
    // middleware: ['meta'],
  },
  /**
   * Transición por defecto
   */
  pageTransition: 'page',
  /**
   * Crea un set para navegadores más vergras
   * https://nuxtjs.org/api/configuration-modern#the-modern-property
   */
  modern: isProd ? 'client' : false,
  /**
   * CSS global
   */
  css: [
    '@fortawesome/fontawesome-svg-core/styles.css',
    '~assets/sass/transitions.scss',
    '~assets/sass/app.scss',
    '~assets/sass/global.scss',
    // NOTE: Aquí puedes modificar el estilo de fontawesome
    // '@fortawesome/fontawesome-pro/css/fontawesome.css',
    // '@fortawesome/fontawesome-pro/css/light.css',
    // '@fortawesome/fontawesome-pro/css/regular.css',
    // '@fortawesome/fontawesome-pro/css/solid.css',
    // '@fortawesome/fontawesome-pro/css/brands.css',
  ],
  /*
   ** Customize the progress bar color
   */
  loading: {
    color: '#3B8070',
  },
  /**
   * Nuxt Plugins
   */
  plugins: [
    '~/plugins/global-components.js',
    '~/plugins/font-awesome.js',
    '~/plugins/gtag.js',
    '~plugins/buefy.js',
    {
      src: '~plugins/scroll-track.js',
      nossr: true,
    },
  ],
  /*
   ** Build configuration
   */
  build: {
    /**
     * Enable thread-loader in webpack building
     */
    // parallel: !isProd,
    /**
     * Enable cache of terser-webpack-plugin and cache-loader
     */
    // cache: !isProd,
    /**
     * Es necesario sobreescribir lo que hace babel por defecto
     */
    babel: {
      presets: ({ isServer }) => [
        [
          require.resolve('@nuxt/babel-preset-app'),
          {
            buildTarget: isServer ? 'server' : 'client',
            // Incluir polyfills globales es mejor que no hacerlo
            useBuiltIns: 'entry',
            // Un poco menos de código a cambio de posibles errores
            loose: true,
            // Nuxt quiere usar ie 9, yo no.
            targets: isServer
              ? {
                  node: 'current',
                }
              : {},
          },
        ],
      ],
      plugins: [
        // Reduce drásticamente el tamaño del bundle
        'lodash',
        // Si usas useBuiltIns: usage, descomenta el sig código
        // [
        //   '@babel/plugin-transform-runtime',
        //   {
        //     regenerator: true,
        //   },
        // ],
      ],
    },
    // Hace el css cacheable
    extractCSS: isProd,
    // Alias el ícono de buefy a uno que soporta los íconos de font-awesome
    plugins: [
      new (require('webpack')).NormalModuleReplacementPlugin(
        /buefy\/src\/components\/icon\/Icon\.vue/,
        require.resolve('./components/Buefy/Icon.vue')
      ),
      new (require('lodash-webpack-plugin'))(),
    ],
    /**
     * @param {object} config The Build Config
     * @param {object} ctx Nuxt Context
     * @returns {object} config
     */
    extend(config, { isDev, isClient }) {
      if (isDev) {
        config.devtool = '#source-map'
      }
      // Evita conflictos con el bloque de documentación
      config.module.rules.push({
        resourceQuery: /blockType=docs/,
        loader: require.resolve('./utils/docs-loader.js'),
      })
      // Arregla pnpm
      function replaceLoaders(use) {
        if (use.loader) {
          if (use.loader.indexOf('babel-loader') !== -1) {
            use.loader = 'babel-loader'
          }
        } else if (use.indexOf('babel-loader') !== -1) {
          // eslint-disable-next-line no-param-reassign
          use = 'babel-loader'
        }
      }
      config.module.rules.forEach(rule => {
        if (rule.use) {
          rule.use.forEach(replaceLoaders)
        }
        if (rule.oneOf) {
          rule.oneOf.forEach(oneOf => {
            oneOf.use.forEach(replaceLoaders)
          })
        }
      })
      // Run ESLINT on save
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules(?!\/buefy)$/,
        })
      }
      return config
    },
  },
  /**
   * Estos modulos son altamente recomendado en producción ya que facilitan
   * la incluisión de varias herramientas
   */
  modules: [
    '@dinamomx/nuxtent',
    /**
     * Limpiador de CSS
     */
    'nuxt-purgecss',
    /**
     * Faebook pixel
     */
    [
      require.resolve('./modules/facebook-pixel'),
      {
        id: '556055495177050',
        debug: !isProd,
        disable: false,
      },
    ],

    /**
     * PWA Este módulo integra todo lo necesario para implementar las
     * capacidades PWA a una página web, require ssl
     */
    [
      '@nuxtjs/pwa',
      {
        workbox: {
          generateSW: true,
          // importScripts: ['/notifications-worker.js'],
          InjectManifest: true,
        },
      },
    ],
  ],
  /**
   * Configuración para PurgeCSS
   */
  purgeCSS: {
    whitelist: [
      'html',
      'body',
      'input',
      'help',
      'help.is-danger',
      'input.is-danger',
      'fal',
      'fa-exclamation-circle',
      'fa-lg',
      'has-text-danger',
      'has-icons-right',
      'modal-background',
      'animation-content ',
      'modal-content',
      'svg-inline--fa',
      'tabs',
    ],
    whitelistPatterns: [
      /loading-overlay/,
      /fa-.*/,
      /[\w|-]+-(enter|leave|move)-?(active|to)?/g,
    ],
    whitelistPatternsChildren: [
      /loading-overlay$/,
      /fa-exclamation-circle$/,
      /has-icons-$/,
      /^select/,
      /^tabs/,
    ],
  },
  /**
   * Configuraciones que generan automáticamente manifest y
   * etiquetas básicas para seo
   */
  manifest: {
    name: 'SummerHillls Kinder y Nursery House',
    short_name: 'SummerHillls',
    start_url: '../promociones/formato-digital/',
    display: 'fullscreen',
  },
  meta: {
    name: 'SummerHillls Kinder y Nursery House',
    author: 'https://dinamo.mx',
    description:
      // eslint-disable-next-line max-len
      'Kinder y guardería bilingüe ubicada en la Condesa. Con más de 30 años de experiencia, nos concentramos en la educación emocional, inculcar valores y fomentar la felicidad de los niños.',
    theme_color: '#002c55',
    lang: 'es',
    nativeUI: false,
    mobileApp: false,
    appleStatusBarStyle: 'black',
    ogSiteName: true,
    ogTitle: true,
    ogDescription: true,
    ogImage: true,
    ogHost: 'http://summerhills.com.mx',
    ogUrl: true,
    twitterCard: false,
  },
}
